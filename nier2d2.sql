-- Active: 1673947616242@@127.0.0.1@3306@Nier2D2
-- Active: 1673948012430@@127.0.0.1@3306@nier2d2
-- Active: 1673947756043@@127.0.0.1@3306@Nier2D2
-- Active: 1673948012430@@127.0.0.1@3306@nier2d2
-- Active: 1673947756043@@127.0.0.1@3306@Nier2D2
--Liens à changer en fonction de notre BDD

--Sam
--Liens à changer en fonction de notre BDD
--CREATE DATABASE Nier2D2;
DROP TABLE IF EXISTS session_student;
DROP TABLE IF EXISTS session_formateur;
DROP TABLE IF EXISTS signature;
DROP TABLE IF EXISTS formations;
DROP TABLE IF EXISTS course;
DROP TABLE IF EXISTS brief;
DROP TABLE IF EXISTS rendu;
DROP TABLE IF EXISTS module;
DROP TABLE IF EXISTS formateur;
DROP TABLE IF EXISTS student;
DROP TABLE IF EXISTS contact;
DROP TABLE IF EXISTS session;

CREATE TABLE session(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR (255) NOT NULL,
    dateDebut DATE NOT NULL,
    dateFin DATE NOT NULL
);

CREATE TABLE contact(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
    mail VARCHAR(255)
);

CREATE TABLE student(
    id INT PRIMARY KEY AUTO_INCREMENT,
    surname VARCHAR(250) NOT NULL,
    lastname VARCHAR(250) NOT NULL,
    birthdate DATE,
    gender VARCHAR(250) NOT NULL,
    teletravail BOOLEAN
);

CREATE TABLE formateur(
    id INT PRIMARY KEY AUTO_INCREMENT,
    surname VARCHAR(255) NOT NULL,
    firstname VARCHAR(255) NOT NULL,
    birthdate DATE NOT NULL,
    skills VARCHAR(255)
);

CREATE TABLE module(
    id INT PRIMARY KEY AUTO_INCREMENT,
    module VARCHAR(255) NOT NULL,
    horaire DATE,
    id_session INT,
    Foreign Key (id_session) REFERENCES session (id)
);


CREATE TABLE rendu(
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(250) NOT NULL,
    content VARCHAR(250) NOT NULL,
    evaluate DATE,
    id_student INT,
    Foreign Key (id_student) REFERENCES student(id)
);

CREATE TABLE brief(
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(250) NOT NULL,
    id_session INT,
    Foreign Key (id_session) REFERENCES session(id)
);


CREATE TABLE course(
    id INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(255),
    content VARCHAR(255),
    comment VARCHAR(255),
    id_session INT,
    Foreign Key (id_session) REFERENCES session(id)
);


CREATE TABLE formations(
    id INT PRIMARY KEY AUTO_INCREMENT,
    type VARCHAR(255) NOT NULL,
    date DATE NOT NULL,
    contenu VARCHAR(255),
    id_session INT,
    id_contact INT,
    Foreign Key (id_session) REFERENCES session(id),
    Foreign Key (id_contact) REFERENCES contact(id)
);

CREATE TABLE signature(
    id INT PRIMARY KEY AUTO_INCREMENT,
    signed BOOLEAN,
    id_student INT,
    id_session INT,
    id_formation INT,
    Foreign Key (id_student) REFERENCES student(id),
    Foreign Key (id_session) REFERENCES session(id),
    Foreign Key (id_formation) REFERENCES formations(id)
);

CREATE TABLE session_student(
    session_id INT,
    student_id INT,
    PRIMARY KEY (session_id, student_id),
    Foreign Key (session_id) REFERENCES session(id),
    Foreign Key (student_id) REFERENCES student(id)
);

CREATE TABLE session_formateur(
    session_id INT,
    formateur_id INT,
    PRIMARY KEY (session_id, formateur_id),
    Foreign Key (session_id) REFERENCES session(id),
    Foreign Key (formateur_id) REFERENCES formateur(id)
); 

INSERT INTO module(module,horaire) VALUES
('Combat','2023-01-10'),
('Dance','2023-01-23'),
('Cuisine','2023-01-30');

INSERT INTO formations(`date`,`type`,contenu) VALUES
('2023-01-02','chaudronnier','Le chaudronnier donne forme aux feuilles de métal, puis il les assemble afin de réaliser les produits les plus variés'),
('2023-01-02','type','contenu'),
('2023-01-02','type','contenu');

INSERT INTO signature(signed) VALUES
(0),(1),(0);

INSERT INTO session (name, `dateDebut`,`dateFin`) VALUES 
('Replicant','1023-01-02','1123-01-02'),
('2B','2023-01-02','2123-01-02'),
('9S','2023-01-02','2123-01-02');

INSERT INTO contact(name, mail) VALUES
('Mélanie', 'Mélamail@mail.com');

INSERT INTO course (title, content,comment) VALUES
('Manuel', 'Lire un manuel', 'C\'est pour les nuls'),
('IA', 'Créer une IA', 'C\'est le meilleur cours');

INSERT INTO formateur (surname, firstname, birthdate, skills) VALUES
('Jean-Morc', 'Demel', '1990-02-01', 'IA'),
('Jean-Pascal', 'Medel', '1960-12-01', 'Robotique'),
('Jean-Mouloude', 'emeld', '1980-04-01', 'META'),
('Jean-Rama', 'melde', '1989-04-23', 'Fonte de métal');

INSERT INTO student(surname, lastname,birthdate, gender, teletravail) VALUES
('Samantha', 'Grobon', '1992-04-12', 'Femme', true),
('Alexandre','Noens','2000-01-27','Homme', false),
('Yann','Pezevan','1999-08-14', 'Homme', false),
('Hisame', 'Stolz', '1998-02-23','Femme', true),
('Axel', 'Reviron','1998-07-13','Homme', true),
('Melanie', 'Ferer','1989-06-15','Femme', true),
('Marie', 'Benitah','1992-06-16','Femme', false),
('Laura', 'Gotti','1990-03-24','Femme', false),
('Nicolas', 'Harizia','1996-06-19','Homme', true),
('Judith', 'Sévy','1996-08-31','Femme', true),
('Ellyesse', 'Daoudi','2000-02-23','Homme', true),
('Laure', 'Glaizal','2001-05-11','Femme', false),
('Juliette', 'Suc','2010-01-23','Femme', true),
('Chems', 'Meziou','1990-06-08','Homme', true),
('Hajar', 'Zahoui','1990-06-08','Femme', true);

INSERT INTO rendu(title, content,evaluate, id_student) VALUES
('Projet 1', 'Typescript', '2022-12-30', 1);

INSERT INTO brief(title, id_session) VALUES
('Compte rendu', 1);

INSERT INTO session_student(session_id, student_id) VALUES
('1','1'),('1','2'),('1','3'),('1','4'),('1','5'),
('2','6'),('2','7'),('2','8'),('2','9'),('2','10'),
('3','11'),('3','12'),('3','13'),('3','14'),('3','15');
INSERT INTO session_formateur(session_id, formateur_id) VALUES
('1','1'),('2','2'),('3','3'),('3','4');