#Projet SQL - Training Center (Centre de formation)


#Étapes du projet

On a commencé par créer un usercase puis le diagramme de classe afin de visualiser et de lister les besoins.
On a créé l'architecture du projet puis le fichier SQL. Fichier contenant les différentes créations de table et l'insertion de données.
Ensuite nous avons fait la méthode CRUD pour les class du Repository et enfin nous avons, en groupe, réalisé les méthodes complexes.

#Organisation 

Nous avons réparti les tâches entre Chems, Alexandre et Samantha avec les issues de GITLabs.
Chaque début d'après-midi on se concertait afin de définir qui faisait certaines tâches, pour les fonctions complexes, le travail de groupe à été privilégier (exemple : table de jointure).

#Images de l'user-case et du diagramme des classes

Image du diagramme des classes: 
![<alt>](</public/img/diagram.jpg>)

Image du l'usercase:
![<alt>](</public/img/usercase.jpg>)

