
<?php
use App\Entities\Session;
use App\Entities\Courses;
use App\Entities\Student;
use App\Repository\Database;
use App\Repository\SessionRepository;
use App\Repository\StudentRepository;
use App\Repository\CoursesRepository;

require('../vendor/autoload.php');
$connection = Database::connect();
 

//alexandre
$sessions = new SessionRepository();
//Ajoute une nouvelle Sessions
// var_dump($firstSession, $secondSession);

// $sessionsOne = new Session("A2",new DateTime(), new DateTime());
// $sessions->persist($sessionsOne);

//Rename la session 3 par "Emile"
// $sessions->update(3, "Emile");

// Supprime la session 4
// $sessions->delete(4);

//Affiche toutes les sessions
//var_dump($sessions->findAll());

//Affiche les sessions en fonction de l'id
//var_dump($sessions->findById(1), $sessions->findById(4012));

// affiche les sesions en cours
//var_dump($sessions->isStillHere());

//Affiche tous les élèves de la session 1
// var_dump($sessions->findBySession_Student(1));
// var_dump($sessions->findBySession_Formateur(3));

//Affiche tous les formateur de la session 3
var_dump($sessions->findBySession_Formateur(3));
var_dump($sessions->findBySession_Student(1));



// Samantha

$courses = new CoursesRepository();

//Permet de modifier le course et de le lier à une session
// $courses->update(1, 'windows', 'Pour les nulls', 'les gros nulls', 2);

//Permet de supprimer un course
// $courses->delete(1);

//Créer un course et le fait persister
// $courses2B = new Courses('Conception Robot', 'Création de robot', 'cours pour débutant' );
// $courses->persist($courses2B);
// $coursesIA = new Courses('Apple', 'Les Macs pour les nulls', 'oskour' );
// $courses->persist($coursesIA);


//cette ligne me sert à voir correctement dans google ne pas supprimer !
// echo '<pre>' . var_export($courses->findAll(), true) . '</pre>';

//Chems
$students = new StudentRepository();

// $student1 = new Student("Rama", "Soumare", new DateTime("1975-05-13"),"Femme", false);
// $students->persist($student1);
$students->update(1, "Samanta", "Grobon", new DateTime("1975-02-23"), "Femme", true);

// var_dump($students->findAll());
// var_dump($students->findById(1));
// var_dump($students->findById(42));
// var_dump($students->studentOnRemote());

