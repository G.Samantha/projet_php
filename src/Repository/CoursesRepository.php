<?php

namespace App\Repository;
use App\Entities\Courses;
use PDO;
use DateTime;

class CoursesRepository{
        private PDO $connection;

    /**
     * Permet la connexion
     */
    public function __construct() {
        $this->connection = Database::connect();
    }

    /**
     * Affiche toutes les courses
     * @return array
     */
    public function findAll() : array {

        $courses = [];
        $statement = $this->connection->prepare('SELECT * FROM course');
        $statement->execute();
        $result = $statement->fetchAll();

        foreach ($result as $key) {
            $courses[] = new Courses($key['title'], $key['content'], $key['comment'], $key['id_session'], $key['id']);
        }
        return $courses;
    }

    /**
     * Ajoute une courses à la BDD
     * @param Courses $courses
     */
    public function persist(Courses $courses) {
        $statement = $this->connection->prepare('INSERT INTO course (title,content,comment) VALUES (:title, :content, :comment)');
        $statement->bindValue('title', $courses->getTitle());
        $statement->bindValue('content', $courses->getContent());
        $statement->bindValue('comment', $courses->getComment());

        $statement->execute();
        $courses->setId($this->connection->lastInsertId());

    }

    /**
     * Affiche une course en fonction de l'id
     * @param int $id
     * @return Courses|null
     */
    public function findById(int $id):?Courses {
        $statement = $this->connection->prepare('SELECT * FROM course WHERE id=:id');
        $statement->bindValue('id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToCourses($result);
        }
        return null;
    }

    /**
     * Méthode qui prend une ligne de résultat PDO et la convertit en instance de Courses
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return Courses l'instance de Courses
     */
    private function sqlToCourses(array $line):Courses {
        return new Courses($line['title'], $line['content'], $line['comment'], $line['id'],  $line['id_session']);
    }


    /**
     * Met à jours une course
     * @param int $id
     * @param string $title
     * @param string $content
     * @param string $comment
     * @param int $idSession
     * @return void
     */
    public function update(int $id, string $title, string $content, string $comment, int $idSession):void {

        $statement = $this->connection->prepare("UPDATE course SET title=:title, content=:content, comment=:comment, id_session=:id_session  WHERE id=:id ");
        $statement->bindValue(":id", $id);
        $statement->bindValue(":title", $title);
        $statement->bindValue(":content", $content);
        $statement->bindValue(":comment", $comment);
        $statement->bindValue(":id_session", $idSession);

        $statement->execute();

     }

     /**
      * Supprime une course en fonction de l'id
      * @param int $id
      * @return void
      */
     public function delete(int $id)
     {
        $statement = $this->connection->prepare('DELETE FROM course WHERE id = :id');
        $statement->bindValue('id', $id);
        $statement->execute();
     }
 

}