<?php

namespace App\Repository;

use App\Entities\Student;
use DateTime;
use PDO;

/**
 * StudentRepository est la class qui nous permet de créer 
 * toutes les méthodes liées à l'objet Student
 */
class StudentRepository
{
    private PDO $connection;

    public function __construct()
    {
        $this->connection = Database::connect();
    }

    /**
     *Summary of findAll, est la méthode qui nous permet d'afficher tous les étudiants qui
     *sont dans la table student 
     * @return array
     */
    public function findAll(): array
    {
        $students = [];
        $statement = $this->connection->prepare('SELECT * FROM student');
        $statement->execute();
        $result = $statement->fetchAll();

        foreach ($result as $key) {
            $birthdate = null;
            if (isset($key['birthdate'])) {
                $birthdate = new DateTime($key['birthdate']);
            }
            $students[] = new Student($key['surname'], $key['lastname'], $birthdate, $key['gender'], $key['teletravail'], $key['id']);
        }
        return $students;
    }

    /**
     * Summary of findById, est la function qui nous affiche les étudiant par leur id
     * @param int $id
     * @return Student|null
     */
    public function findById(int $id): Student|null
    {
        $statement = $this->connection->prepare('SELECT * FROM student WHERE id = :id');
        $statement->bindValue('id', $id);
        $statement->execute();
        $results = $statement->fetch();

        $birthdate = null;
        if (isset($results['birthdate'])) {
            $birthdate = new DateTime($results['birthdate']);
        }
        if ($results) {
            return new Student($results['surname'], $results['lastname'], $birthdate, $results['gender'], $results['teletravail'], $results['id']);
        }

        return null;
    }

    /**
     * Summary of studentOnRemote,la function qui nous affiche les étudiants qui 
     * sont en télétravail
     * @return array<Student>
     */
    public function studentOnRemote()
    {
        $remoteStudents = [];
        $statement = $this->connection->prepare('SELECT * FROM student WHERE teletravail=true');
        $statement->execute();
        $result = $statement->fetchAll();

        foreach ($result as $key) { 
                $birthdate = null;
            if (isset($key['birthdate'])) {
                $birthdate = new DateTime($key['birthdate']);
            }
            $remoteStudents[] = new Student($key['surname'], $key['lastname'], $birthdate, $key['gender'], $key['teletravail'], $key['id']);
        }
        return $remoteStudents;
    }
    /**
     * Summary of persist, la function qui fait persisté un étudiant dans la table student
     * @param Student $students
     * @return void
     */
    public function persist(Student $students)
    {
        $statement = $this->connection->prepare('INSERT INTO student (surname, lastname, birthdate, gender, teletravail) VALUES (:surname, :lastname, :birthdate, :gender, :teletravail)');

        $statement->bindValue('surname', $students->getSurname());
        $statement->bindValue('lastname', $students->getLastname());
        // $birthdate = null;
        $statement->bindValue('birthdate', $students->getBirthdate()->format('Y-m-d'));
        $statement->bindValue('gender', $students->getGender());
        $statement->bindValue('teletravail', $students->getTeletravail(), PDO::PARAM_BOOL);
        $students->setId($this->connection->lastInsertId());
        $statement->execute();
    }

    /**
     * Summary of update, la function qui met à jours un étudiant précis dans la 
     * table student 
     * @param int $id
     * @param string $surname
     * @param string $lastname
     * @param DateTime $birthdate
     * @param string $gender
     * @param bool $teletravail
     * @return void
     */
    public function update(int $id, string $surname, string $lastname, DateTime $birthdate, string $gender, bool $teletravail)
    {
        $statement = $this->connection->prepare('UPDATE student SET surname = :surname, lastname = :lastname, birthdate = :birthdate, gender = :gender, teletravail= :teletravail WHERE id = :id');
        $statement->bindValue('id', $id);
        $statement->bindValue("surname", $surname);
        $statement->bindValue("lastname", $lastname);
        $statement->bindValue("birthdate", $birthdate->format('Y-m-d'));
        $statement->bindValue("gender", $gender);
        $statement->bindValue("teletravail", $teletravail);

        $statement->execute();
    }
// public function delete(int $id) {
//     $statement = $this->connection->prepare('DELETE FROM student WHERE id = :id');
//     $statement->bindValue('id', $id);
//     $statement->execute();
// }
}