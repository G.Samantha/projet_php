<?php

namespace App\Repository;

use App\Entities\Session;
use App\Entities\Student;
use PDO;
use DateTime;

class SessionRepository{
    private PDO $connection;

    /**
     * Permet la connexion
     */
    public function __construct() {
        $this->connection = Database::connect();
    }

    /**
     * Affiche toutes les sessions qui sont actuellement en cours et 
     * @return $sessions[]
     */
    public function isStillHere() : array {

        $laDateActuelle = new DateTime;

        $sessions = [];

        $statement = $this->connection->prepare('SELECT * FROM session WHERE NOW() BETWEEN dateDebut AND dateFin');

        $statement->execute();

        $result = $statement->fetchAll();

        foreach ($result as $key) {
            $dateDebut = new DateTime($key['dateDebut']); 
            $dateFin = new DateTime($key['dateFin']);
            $sessions[] = new Session($key['name'],$dateDebut,$dateFin, $key['id']);
        }


        return $sessions;
    } 

    /**
     * Affiche toutes les sessions
     * @return array
     */
    public function findAll() : array {

        $sessions = [];

        $statement = $this->connection->prepare('SELECT * FROM session');

        $statement->execute();

        $result = $statement->fetchAll();

        foreach ($result as $key) {
            $dateDebut = new DateTime($key['dateDebut']); 
            $dateFin = new DateTime($key['dateFin']);
            $sessions[] = new Session($key['name'],$dateDebut,$dateFin, $key['id']);
        }

        return $sessions;   

    }

    /**
     * Affiche la session correspondant à l'id donné.
     * @param int $id
     * @return Session|null
     */
    public function findById(int $id) : Session | null {

        $statement = $this->connection->prepare('SELECT * FROM session WHERE id = :id ');
        $statement->bindValue('id', $id);

        $statement->execute();

        $results = $statement->fetch();

        if($results) {
            $dateDebut = new DateTime($results['dateDebut']); 
            $dateFin = new DateTime($results['dateFin']);
            return new Session($results['name'],$dateDebut, $dateFin, $results['id']);
        }
        
        return null;

    }

    /**
     * Ajoute une session à la Base de Donnée
     * @param Session $session
     */
    public function persist(Session $session){
        
        $statement = $this->connection->prepare('INSERT INTO session (name) VALUES ( :name )');

        $statement->bindValue('name', $session->getName());

        $statement->execute();

        $session->setId($this->connection->lastInsertId());

    }

    /**
     * Met à jours le nom d'une session via l'id.
     * @param int $id
     * @param string $name
     */
    public function update(int $id, string $name) {
        $statement = $this->connection->prepare('UPDATE session SET name = :name WHERE id = :id');
        $statement->bindValue('id', $id);
        $statement->bindValue("name", $name);

        $statement->execute();
    }

    /**
     * Supprime la session en donnant l'id
     * @param int $id
     * @return void
     */
    public function delete(int $id) {
        $statement = $this->connection->prepare('DELETE FROM session WHERE id = :id');
        $statement->bindValue('id', $id);
        $statement->execute();
    }
    /**
     * Affiche tout les élèves d'une session grace à l'id donné
     * @param int $idSession
     * @return array
     */
    public function findBySession_Student(int $idSession) {
        
        $statement = $this->connection->prepare('SELECT name,session_id, student_id, lastname, surname,birthdate,gender 
        FROM session_student 
        JOIN student s ON s.id=student_id 
        JOIN session nsm ON nsm.id=session_id  
        WHERE  session_id = :idSession');

        $statement->bindValue('idSession', $idSession);
        $statement->execute();
        $result = $statement->fetchAll();
        return $result;
    }

    /**
     * Summary of findBySession_Formateur, function qui affiche les formateurs liées au sessions
     * @param int $idSession
     * @return array
     */
    public function findBySession_Formateur(int $idSession){
        $statement = $this->connection->prepare('SELECT session_id, formateur_id, surname, firstname, skills FROM session_formateur JOIN formateur f ON f.id=formateur_id WHERE session_id=:idSession');

        $statement->bindValue('idSession', $idSession);
        $statement->execute();
        $result = $statement->fetchAll();
        return $result;
    }
}
