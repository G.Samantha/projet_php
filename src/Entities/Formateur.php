<?php

namespace App\Entities;

use DateTime;

class Formateur{
    private int $id;
    private string $surname;
    private string $firstname;
    private DateTime $birthdate;
    private array $skills;
    


    /**
     * @param int $id
     * @param string $surname
     * @param string $firstname
     * @param DateTime $birthdate
     * @param array $skills
     */
    public function __construct(int $id, string $surname, string $firstname, DateTime $birthdate, array $skills) {
    	$this->id = $id;
    	$this->surname = $surname;
    	$this->firstname = $firstname;
    	$this->birthdate = $birthdate;
    	$this->skills = $skills;
    }

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}
	
	/**
	 * @param int $id 
	 * @return self
	 */
	public function setId(int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getSurname(): string {
		return $this->surname;
	}
	
	/**
	 * @param string $surname 
	 * @return self
	 */
	public function setSurname(string $surname): self {
		$this->surname = $surname;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getFirstname(): string {
		return $this->firstname;
	}
	
	/**
	 * @param string $firstname 
	 * @return self
	 */
	public function setFirstname(string $firstname): self {
		$this->firstname = $firstname;
		return $this;
	}
	
	/**
	 * @return DateTime
	 */
	public function getBirthdate(): DateTime {
		return $this->birthdate;
	}
	
	/**
	 * @param DateTime $birthdate 
	 * @return self
	 */
	public function setBirthdate(DateTime $birthdate): self {
		$this->birthdate = $birthdate;
		return $this;
	}
	
	/**
	 * @return array
	 */
	public function getSkills(): array {
		return $this->skills;
	}
	
	/**
	 * @param array $skills 
	 * @return self
	 */
	public function setSkills(array $skills): self {
		$this->skills = $skills;
		return $this;
	}
}