<?php

namespace App\Entities;

class Briefs{
    private int $id;
    private string $title;
    /**
     * @param int $id
     * @param string $title
     */
    public function __construct(int $id, string $title) {
    	$this->id = $id;
    	$this->title = $title;
    }

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}
	
	/**
	 * @param int $id 
	 * @return self
	 */
	public function setId(int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getTitle(): string {
		return $this->title;
	}
	
	/**
	 * @param string $title 
	 * @return self
	 */
	public function setTitle(string $title): self {
		$this->title = $title;
		return $this;
	}
}