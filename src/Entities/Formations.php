<?php

namespace App\Entities;

use DateTime;

class Formations{
    private int $id;
    private string $type;
    private DateTime $date;
    private string $contenu;
    private int $idSession;
    private int $idContact;


    /**
     * @param int $id
     * @param string $type
     * @param DateTime $date
     * @param string $contenu
     * @param int $idSession
     * @param int $idContact
     */
    public function __construct(int $id, string $type, DateTime $date, string $contenu, int $idSession, int $idContact) {
    	$this->id = $id;
    	$this->type = $type;
    	$this->date = $date;
    	$this->contenu = $contenu;
    	$this->idSession = $idSession;
    	$this->idContact = $idContact;
    }

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}
	
	/**
	 * @param int $id 
	 * @return self
	 */
	public function setId(int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getType(): string {
		return $this->type;
	}
	
	/**
	 * @param string $type 
	 * @return self
	 */
	public function setType(string $type): self {
		$this->type = $type;
		return $this;
	}
	
	/**
	 * @return DateTime
	 */
	public function getDate(): DateTime {
		return $this->date;
	}
	
	/**
	 * @param DateTime $date 
	 * @return self
	 */
	public function setDate(DateTime $date): self {
		$this->date = $date;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getContenu(): string {
		return $this->contenu;
	}
	
	/**
	 * @param string $contenu 
	 * @return self
	 */
	public function setContenu(string $contenu): self {
		$this->contenu = $contenu;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getIdSession(): int {
		return $this->idSession;
	}
	
	/**
	 * @param int $idSession 
	 * @return self
	 */
	public function setIdSession(int $idSession): self {
		$this->idSession = $idSession;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getIdContact(): int {
		return $this->idContact;
	}
	
	/**
	 * @param int $idContact 
	 * @return self
	 */
	public function setIdContact(int $idContact): self {
		$this->idContact = $idContact;
		return $this;
	}
}