<?php

namespace App\Entities;

use DateTime;

class Session
{
    private ?int $id;
    private string $name;
	private DateTime $dateDebut;
	private DateTime $dateFin;

	

	/**
	 * @param int|null $id
	 * @param string $name
	 * @param DateTime $dateDebut
	 * @param DateTime $dateFin
	 */
	public function __construct(string $name, DateTime $dateDebut, DateTime $dateFin, ?int $id = null) {
		$this->id = $id;
		$this->name = $name;
		$this->dateDebut = $dateDebut;
		$this->dateFin = $dateFin;
	}

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
	
	/**
	 * @param string $name 
	 * @return self
	 */
	public function setName(string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return DateTime
	 */
	public function getDateDebut(): DateTime {
		return $this->dateDebut;
	}
	
	/**
	 * @param DateTime $dateDebut 
	 * @return self
	 */
	public function setDateDebut(DateTime $dateDebut): self {
		$this->dateDebut = $dateDebut;
		return $this;
	}
	
	/**
	 * @return DateTime
	 */
	public function getDateFin(): DateTime {
		return $this->dateFin;
	}
	
	/**
	 * @param DateTime $dateFin 
	 * @return self
	 */
	public function setDateFin(DateTime $dateFin): self {
		$this->dateFin = $dateFin;
		return $this;
	}
}