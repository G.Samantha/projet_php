<?php

namespace App\Entities;

use DateTime;

class Contact {
    private int $id;
    private string $name;
    private string $mail;


    /**
     * @param int $id
     * @param string $name
     * @param string $mail
     */
    public function __construct(int $id, string $name, string $mail) {
    	$this->id = $id;
    	$this->name = $name;
    	$this->mail = $mail;
    }

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}
	
	/**
	 * @param int $id 
	 * @return self
	 */
	public function setId(int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
	
	/**
	 * @param string $name 
	 * @return self
	 */
	public function setName(string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getMail(): string {
		return $this->mail;
	}
	
	/**
	 * @param string $mail 
	 * @return self
	 */
	public function setMail(string $mail): self {
		$this->mail = $mail;
		return $this;
	}
}
    