<?php

namespace App\Entities;

use DateTime;

class Rendu{
    private int $id;
    private string $title;
    private string $content;
    private DateTime $evaluate;

    /**
     * @param int $id
     * @param string $title
     * @param string $content
     * @param DateTime $evaluate
     */
    public function __construct(int $id, string $title, string $content, DateTime $evaluate) {
    	$this->id = $id;
    	$this->title = $title;
    	$this->content = $content;
    	$this->evaluate = $evaluate;
    }

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}
	
	/**
	 * @param int $id 
	 * @return self
	 */
	public function setId(int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getTitle(): string {
		return $this->title;
	}
	
	/**
	 * @param string $title 
	 * @return self
	 */
	public function setTitle(string $title): self {
		$this->title = $title;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getContent(): string {
		return $this->content;
	}
	
	/**
	 * @param string $content 
	 * @return self
	 */
	public function setContent(string $content): self {
		$this->content = $content;
		return $this;
	}
	
	/**
	 * @return DateTime
	 */
	public function getEvaluate(): DateTime {
		return $this->evaluate;
	}
	
	/**
	 * @param DateTime $evaluate 
	 * @return self
	 */
	public function setEvaluate(DateTime $evaluate): self {
		$this->evaluate = $evaluate;
		return $this;
	}
}