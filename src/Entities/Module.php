<?php

namespace App\Entities;

use DateTime;

class Module {
    private int $id;
    private string $module;
    private DateTime $horaire;
    private int $idSession;
    

    /**
     * @param int $id
     * @param string $module
     * @param DateTime $horaire
     * @param int $idSession
     */
    public function __construct(int $id, string $module, DateTime $horaire, int $idSession) {
    	$this->id = $id;
    	$this->module = $module;
    	$this->horaire = $horaire;
    	$this->idSession = $idSession;
    }

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}
	
	/**
	 * @param int $id 
	 * @return self
	 */
	public function setId(int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getModule(): string {
		return $this->module;
	}
	
	/**
	 * @param string $module 
	 * @return self
	 */
	public function setModule(string $module): self {
		$this->module = $module;
		return $this;
	}
	
	/**
	 * @return DateTime
	 */
	public function getHoraire(): DateTime {
		return $this->horaire;
	}
	
	/**
	 * @param DateTime $horaire 
	 * @return self
	 */
	public function setHoraire(DateTime $horaire): self {
		$this->horaire = $horaire;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getIdSession(): int {
		return $this->idSession;
	}
	
	/**
	 * @param int $idSession 
	 * @return self
	 */
	public function setIdSession(int $idSession): self {
		$this->idSession = $idSession;
		return $this;
	}
}