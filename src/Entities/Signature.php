<?php

namespace App\Entities;

class Signature {
    private int $id;
    private array $idStudent;
    private bool $signed;
    private int $idSession;
    private int $idFormation;

        /**
     * @param int $id
     * @param array $idStudent
     * @param bool $signed
     * @param int $idSession
     * @param int $idFormation
     */
    public function __construct(int $id, array $idStudent, bool $signed, int $idSession, int $idFormation) {
    	$this->id = $id;
    	$this->idStudent = $idStudent;
    	$this->signed = $signed;
    	$this->idSession = $idSession;
    	$this->idFormation = $idFormation;
    }

	/**
	 * @return int
	 */
	public function getId(): int {
		return $this->id;
	}
	
	/**
	 * @param int $id 
	 * @return self
	 */
	public function setId(int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return array
	 */
	public function getIdStudent(): array {
		return $this->idStudent;
	}
	
	/**
	 * @param array $idStudent 
	 * @return self
	 */
	public function setIdStudent(array $idStudent): self {
		$this->idStudent = $idStudent;
		return $this;
	}

	
	/**
	 * @return int
	 */
	public function getIdSession(): int {
		return $this->idSession;
	}
	
	/**
	 * @param int $idSession 
	 * @return self
	 */
	public function setIdSession(int $idSession): self {
		$this->idSession = $idSession;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getIdFormation(): int {
		return $this->idFormation;
	}
	
	/**
	 * @param int $idFormation 
	 * @return self
	 */
	public function setIdFormation(int $idFormation): self {
		$this->idFormation = $idFormation;
		return $this;
	}

}