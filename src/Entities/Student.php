<?php

namespace App\Entities;
use DateTime;
class Student{
    private ?int $id;
    private string $surname;
    private string $lastname;
    private DateTime $birthdate;
    private string $gender;
	private bool $teletravail;

    /**
     * @param int $id
     * @param string $surname
     * @param string $lastname
     * @param DateTime $birthdate
     * @param string $gender
	 * @param bool $teletravail
     */
    public function __construct(string $surname, string $lastname, DateTime $birthdate, string $gender, bool $teletravail, ?int $id=null) {
    	$this->id = $id;
    	$this->surname = $surname;
    	$this->lastname = $lastname;
    	$this->birthdate = $birthdate;
    	$this->gender = $gender;
		$this->teletravail = $teletravail;
    }

	/**
	 * @return int
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getSurname(): string {
		return $this->surname;
	}
	
	/**
	 * @param string $surname 
	 * @return self
	 */
	public function setSurname(string $surname): self {
		$this->surname = $surname;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getLastname(): string {
		return $this->lastname;
	}
	
	/**
	 * @param string $lastname 
	 * @return self
	 */
	public function setLastname(string $lastname): self {
		$this->lastname = $lastname;
		return $this;
	}
	
	/**
	 * @return DateTime
	 */
	public function getBirthdate(): DateTime {
		return $this->birthdate;
	}
	
	/**
	 * @param DateTime $birthdate 
	 * @return self
	 */
	public function setBirthdate(DateTime $birthdate): self {
		$this->birthdate = $birthdate;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getGender(): string {
		return $this->gender;
	}
	
	/**
	 * @param string $gender 
	 * @return self
	 */
	public function setGender(string $gender): self {
		$this->gender = $gender;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function getTeletravail(): bool {
		return $this->teletravail;
	}
	
	/**
	 * @param bool $teletravail 
	 * @return self
	 */
	public function setTeletravail(bool $teletravail): self {
		$this->teletravail = $teletravail;
		return $this;
	}
}