<?php

namespace App\Entities;

use DateTime;

class Courses
{
	private ?int $id;
	private string $title;
	private string $content;
	private string $comment;
	private ?int $idSession;



	/**
	 * @param int|null $id
	 * @param string $title
	 * @param string $content
	 * @param string $comment
	 * @param int|null $idSession
	 */
	public function __construct(string $title, string $content, string $comment, ?int $idSession=null, ?int $id=null) {
		$this->id = $id;
		$this->title = $title;
		$this->content = $content;
		$this->comment = $comment;
		$this->idSession = $idSession;
	}

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getTitle(): string {
		return $this->title;
	}
	
	/**
	 * @param string $title 
	 * @return self
	 */
	public function setTitle(string $title): self {
		$this->title = $title;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getContent(): string {
		return $this->content;
	}
	
	/**
	 * @param string $content 
	 * @return self
	 */
	public function setContent(string $content): self {
		$this->content = $content;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getComment(): string {
		return $this->comment;
	}
	
	/**
	 * @param string $comment 
	 * @return self
	 */
	public function setComment(string $comment): self {
		$this->comment = $comment;
		return $this;
	}
	
	/**
	 * @return int|null
	 */
	public function getIdSession(): ?int {
		return $this->idSession;
	}
	
	/**
	 * @param int|null $idSession 
	 * @return self
	 */
	public function setIdSession(?int $idSession): self {
		$this->idSession = $idSession;
		return $this;
	}
}